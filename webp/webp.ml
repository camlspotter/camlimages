type reader

type vp8StatusCode =
  | VP8_STATUS_OK
  | VP8_STATUS_OUT_OF_MEMORY
  | VP8_STATUS_INVALID_VP8
  | PARAM_STATUS_BITSTREAM_ERROR
  | VP8_STATUS_UNSUPPORTED_FEATURE
  | VP8_STATUS_SUSPENDED
  | VP8_STATUS_USER_ABORT
  | VP8_STATUS_NOT_ENOUGH_DATA
[@@ocaml.warning "-37"]

external open_for_read : string -> reader * int * int * bool * bool * int = "camlimages_webp_reader_open"
external set_read_buffer : reader -> bytes -> unit = "camlimages_webp_reader_set_buffer"
external read : reader -> vp8StatusCode = "camlimages_webp_reader_read"
external close_for_read : reader -> unit = "camlimages_webp_reader_close"

let load fn _opts =
  let reader, width, height, has_alpha, has_animation, format = open_for_read fn in
  Format.eprintf "open_for_read %s: width %d height %d has_alhpa %b has_animation %b format %d@."
    fn
    width height has_alpha has_animation format;
  let size = width * height * (if has_alpha then 4 else 3) in
  let bytes = Bytes.create size in
  set_read_buffer reader bytes;

  Fun.protect
    ~finally:(fun () -> close_for_read reader)
    @@ fun () ->
    let rec loop () =
      match read reader with
      | VP8_STATUS_OK -> ()
      | VP8_STATUS_SUSPENDED -> loop ()
      | status ->
          Format.eprintf "error %d@." (Obj.magic status);
          failwith "load error"
    in
    loop ();
    if has_alpha then
      Images.Rgba32 (Rgba32.create_with width height [] bytes)
    else
      Images.Rgb24 (Rgb24.create_with width height [] bytes)

let check_header fn =
  try
    let reader, width, height, _has_alpha, _has_animation, _format = open_for_read fn in
    close_for_read reader;
    Images.{ header_width = width;
             header_height = height;
             header_infos = [] }
  with
  | _ -> raise Images.Wrong_file_type

let () = Images.add_methods WebP
  { check_header = check_header;
    load = Some load;
    save = None;
    load_sequence = None;
    save_sequence = None}
