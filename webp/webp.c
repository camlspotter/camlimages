/***********************************************************************/
/*                                                                     */
/*                             CamlImages                              */
/*                                                                     */
/*                     Jun Furuse, DaiLambda, Inc.                     */
/*                                                                     */
/*  Copyright 2024                                                     */
/*                                                                     */
/***********************************************************************/

#include "../config/config.h"
#include "compat.h"

#ifdef HAS_WEBP

#include <caml/mlvalues.h>
#include <caml/alloc.h>
#include <caml/memory.h>
#include <caml/fail.h>

#include "../include/oversized.h"

#include <string.h>
#include <webp/decode.h>
#include <webp/encode.h>
#include <webp/types.h>

CAMLprim value camlimages_webp_reader_open(value name)
{
    CAMLparam1(name);
    CAMLlocal2(res, res1);

    // A) Init a configuration object
    WebPDecoderConfig *configp;
    if ( !(configp = malloc(sizeof(WebPDecoderConfig))) ){
        caml_failwith("failed to allocate WebPDecoderConfig");
    }

    if (!WebPInitDecoderConfig(configp)) {
        free(configp);
        caml_failwith("Failed to initialize WebPDecoderConfig");
    }

    FILE *infile;

    const char* filename = String_val(name);
    
    if ((infile = fopen(filename, "rb")) == NULL) {
        caml_failwith("failed to open webp file");
    }

    uint8_t *buffer = NULL;
    unsigned int bufsize = 0;
    unsigned int loaded = 0;
    VP8StatusCode ret;

    do {
        unsigned int new_bufsize = bufsize * 2;
        new_bufsize = new_bufsize > 256 ? new_bufsize : 256;

        uint8_t* new_buffer = (uint8_t*)malloc(new_bufsize);
        if (!new_buffer) {
            fclose(infile);
            free(configp);
            if (buffer) { free(buffer); }
            caml_failwith("Failed to load WebP header");
        }
        memcpy((void*)new_buffer, (void*)buffer, loaded);
        if (buffer) { free(buffer); }
        buffer = new_buffer;
        bufsize = new_bufsize;
        unsigned int newly_loaded = fread(buffer + loaded, 1, bufsize - loaded, infile);
        if (newly_loaded == 0){
            fclose(infile);
            free(configp);
            free(buffer);
            caml_failwith("Failed to load WebP header: short file");
        }
        loaded += newly_loaded;
        
        // B) optional: retrieve the bitstream's features.
        ret = WebPGetFeatures(buffer, loaded, &configp->input);

    } while (ret == VP8_STATUS_NOT_ENOUGH_DATA);

    if ( ret != VP8_STATUS_OK ){
        fclose(infile);
        free(configp);
        if (buffer) { free(buffer); }
        caml_failwith("Invalid WebP header");
    }

    fseek(infile, 0L, SEEK_SET);

    res1 = caml_alloc_tuple(3);
    Store_field(res1, 0, Val_ptr(infile));
    Store_field(res1, 1, Val_ptr(configp));
    Store_field(res1, 2, Val_ptr(NULL));
    
    res = caml_alloc_tuple(6);
    Store_field(res, 0, res1);
    Store_field(res, 1, Val_int(configp->input.width));
    Store_field(res, 2, Val_int(configp->input.height));
    Store_field(res, 3, Val_int(configp->input.has_alpha));
    Store_field(res, 4, Val_int(configp->input.has_animation));
    Store_field(res, 5, Val_int(configp->input.format));

    CAMLreturn(res);
}

CAMLprim void camlimages_webp_reader_set_buffer(value reader, value bytes)
{
    CAMLparam2(reader, bytes);

    FILE *infile = Ptr_val(Field(reader, 0));
    WebPDecoderConfig* const configp = Ptr_val(Field(reader, 1));
    WebPIDecoder* idec = Ptr_val(Field(reader, 2));

    if (idec) {
        caml_failwith("WebP reader is already initialized");
    }

    // C) Adjust 'config' options, if needed
    // config.options.no_fancy_upsampling = 1;
    // config.options.use_scaling = 1;
    // config.options.scaled_width = scaledWidth();
    // config.options.scaled_height = scaledHeight();

    // D) Specify 'config' output options for specifying output colorspace.
    // // Optionally the external image decode buffer can also be specified.
    // config.output.colorspace = MODE_BGRA;
    // // Optionally, the config.output can be pointed to an external buffer as
    // // well for decoding the image. This externally supplied memory buffer
    // // should be big enough to store the decoded picture.
    // config.output.u.RGBA.rgba = (uint8_t*) memory_buffer;
    // config.output.u.RGBA.stride = scanline_stride;
    // config.output.u.RGBA.size = total_size_of_the_memory_buffer;
    // config.output.is_external_memory = 1;
    configp->output.colorspace = configp->input.has_alpha ? MODE_RGBA : MODE_RGB;
    configp->output.u.RGBA.rgba = (uint8_t*)Bytes_val(bytes);
    configp->output.u.RGBA.stride =
        configp->input.width * (configp->input.has_alpha ? 4 : 3);
    configp->output.u.RGBA.size = caml_string_length(bytes);
    configp->output.is_external_memory = 1;

    // E.2) Decode image incrementally.
    idec = WebPIDecode(NULL, 0, configp);
    if (!idec) {
        caml_failwith("Failed to allocate WebP reader");
    }

    Store_field(reader, 2, Val_ptr(idec));

    CAMLreturn0;
}

CAMLprim void camlimages_webp_reader_close(value reader)
{
    CAMLparam1(reader);

    FILE *infile = Ptr_val(Field(reader, 0));
    WebPDecoderConfig* const configp = Ptr_val(Field(reader, 1));
    // WebPIDecoder* const idec = Ptr_val(Field(reader, 2));

    WebPFreeDecBuffer(&configp->output);
    free(configp);
    fclose(infile);

    CAMLreturn0;
}

CAMLprim value camlimages_webp_reader_read(value reader)
{
    CAMLparam1(reader);

    FILE *infile = Ptr_val(Field(reader, 0));
    WebPDecoderConfig* const configp = Ptr_val(Field(reader, 1));
    WebPIDecoder* const idec = Ptr_val(Field(reader, 2));

    if (!idec) {
        caml_failwith("WebP reader is not initialized");
    }

    uint8_t buffer[1024];
    unsigned int loaded = fread((void*)buffer, 1, 1024, infile);
    
    VP8StatusCode status = WebPIAppend(idec, buffer, loaded);

    CAMLreturn(Val_int(status));
}

#else

CAMLprim value camlimages_webp_reader_open(){ caml_failwith("unsupported"); }
CAMLprim void camlimages_webp_reader_set_buffer(){ caml_failwith("unsupported"); }
CAMLprim void camlimages_webp_reader_close(){ caml_failwith("unsupported"); }

#endif
